package BoxTests
import box.BoxOperator
import common._
import org.scalatest.AsyncFlatSpec

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.util.Random



class BoxFileTest extends AsyncFlatSpec {


  def fixture =
  new {
    val boxOperator = new BoxOperator("nada", "nothing")
    val es = new ElasticSearch
    val test_unique_string : String = Random.alphanumeric.take(10).mkString

      }

  "Uploading a file to Box" should "create a service object in ElasticSearch" in {
    val f = fixture


    val fileInfoFromBox = f.boxOperator.createFile("try" + f.test_unique_string + ".txt", "this is some content BDD_15", "39194464531")


     def verifyNameAndID() : Future[List[check]] = {
       var checks:ListBuffer[check] = ListBuffer.empty[check]
       val elasticSearchResponse = f.es.findFile("try" + f.test_unique_string + ".txt")

       elasticSearchResponse.map(resp => f.es.getFileIdFromResult(resp)) map {
         case Some(id) =>
           checks += new check(s"Expected file id: ${fileInfoFromBox.getID}", s"Found: $id", id == fileInfoFromBox.getID)

         case None =>
           checks += new check(s"Expected file id: ${fileInfoFromBox.getID}", "Found: None", false)
       }

       elasticSearchResponse.map(resp => f.es.getFileNameFromResult(resp)) map {
         case Some(name) =>
           checks += new check(s"Expected file name : ${fileInfoFromBox.getName}", "Found: $name", name === fileInfoFromBox.getName)

         case None =>
           checks += new check(s"Expected file name : ${fileInfoFromBox.getName}", "Found: None", false)
       }

       Future{checks.toList}
     }






    val res = verifications.verify(new Result("verifyNameAndId"), verifyNameAndID)
    res map {result =>
        if (result.testStatus == "GREEN" || result.testStatus == "YELLOW" ) {
          logger.info(s"final test status is: ${result.testStatus}")
          assert(true)
        } else {
          logger.info(s"final test status is: ${result.testConfig.testStatus}")
          assert(false)
        }
      }

    }











}
