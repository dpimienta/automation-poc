package box


import java.io._

import com.box.sdk.StandardCharsets.UTF_8
import com.box.sdk._
import common.{logger, using}

import scala.collection.JavaConverters._
import scala.io.Source

//import play.api.Logger


class BoxOperator(tenant:String, environment:String)
{


  //tenant and environment are not implemented but there as placeholders
var USER_ID = ""

  private val api = connect()

  private def connect() = {
    using(Source.fromResource("config.json", getClass.getClassLoader)) {
      source => {
        val boxConfig = BoxConfig.readFrom(new StringReader(source.mkString))
        val enterpriseConnection = BoxDeveloperEditionAPIConnection.getAppEnterpriseConnection(boxConfig)
        val enterpriseUsers = BoxUser.getAllEnterpriseUsers(enterpriseConnection).asScala.toSeq
        val userId = enterpriseUsers.find(_.getLogin == "ext@firelayers-sbx.com").get.getID
        BoxDeveloperEditionAPIConnection.getAppUserConnection(userId, boxConfig)
      }
    }
  }


  def createFile(fileName: String, content: String, folderID: String): BoxFile#Info = {
    logger.info(s"Creating File $fileName")

    using(new ByteArrayInputStream(content.getBytes(UTF_8))) {
      inputStream => {
        val folder = new BoxFolder(api, folderID)
        folder.uploadFile(inputStream, fileName)
      }
    }
  }

    def createUser(email:String, name:String): BoxUser#Info =
    {
      logger.info(s"Creating user $email")
      BoxUser.createEnterpriseUser(api, email, name)
    }





}