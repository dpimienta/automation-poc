import java.io.{Closeable, IOException}

import org.slf4j.{Logger, LoggerFactory}

package object common {
  val logger: Logger = LoggerFactory.getLogger("automation")

  def using[A <: Closeable, B](resource: A)(f: A => B): B = {
    try {
      f(resource)
    }
    finally {
      try {
        resource.close()
      }
      catch {
        case e: IOException =>
          logger.warn("Exception closing resource", e)
      }
    }
  }
}
