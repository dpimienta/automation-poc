package common
import com.typesafe.config.ConfigFactory

class TestConfig(var timeStarted:Long, var timeAlottedMinutes:Int, var waitIntervalMinutes:Int, var greenThresholdMinutes:Int, var yellowThresholdMinutes:Int, var redThresholdMinutes:Int)
{

  val config = ConfigFactory.load

   def this(){
     this (System.currentTimeMillis(),
       config.getInt("defaultTest.timeAlottedMinutes"),
       config.getInt("defaultTest.waitIntervalMinutes"),
       config.getInt("defaultTest.greenThresholdMinutes"),
       config.getInt("defaultTest.yellowThresholdMinutes"),
      config.getInt("defaultTest.redThresholdMinutes"))
   }


}


