package common

import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.http.search.SearchResponse
import com.typesafe.config.ConfigFactory
import play.api.libs.json.Json

import scala.concurrent.Future




class ElasticSearch {

  private val config = ConfigFactory.load
  private val host: String = config.getString("elasticSearch.host")
  val client = HttpClient(ElasticsearchClientUri(host, 80))


  def findFile(fileName: String): Future[SearchResponse] = {
    logger.info("finding file "+fileName+" in elastic search")
    query("couch.fldb", "doc.fl_metadata.file_title", fileName)
  }

  def getFileIdFromResult(resp: SearchResponse) : Option[String] = {

    try {
      val doc = resp.hits.hits.head.sourceAsString
      val parsed = Json.parse(doc)
      val out = parsed \ "doc" \ "fl_metadata" \\ "raw_so_id"
      Some(out.mkString.replace('"', ' ')replaceAll("\\s", ""))
    }catch{
      case e: NoSuchElementException => println("result is empty. No FileID Found")
        None
    }
  }

  def getFileNameFromResult(resp: SearchResponse) : Option[String] = {

    try {
      val doc = resp.hits.hits.head.sourceAsString
      val parsed = Json.parse(doc)
      val out = parsed \ "doc" \ "fl_metadata" \\ "file_title"
      Some(out.mkString.replace('"', ' ')replaceAll("\\s", ""))
    }catch{
      case e: NoSuchElementException => println("result is empty. No FileName Found")
        None
    }
  }

  def findUser(email: String): Future[SearchResponse] = {
    logger.info("finding user "+email+" in elastic search")
    query("couch.fldb", "doc.corporate_id.id", email)
  }

  def getUserEmailFromResult(resp: SearchResponse) : Option[String] = {

    try {
      val doc = resp.hits.hits.head.sourceAsString
      val parsed = Json.parse(doc)
      val out = parsed \ "doc" \ "corporate_id" \\ "id"
      Some(out.mkString.replace('"', ' ') replaceAll("\\s", ""))
    } catch {
      case e: NoSuchElementException => println(e.getMessage)
      None
    }
  }


  private def query(index: String, field: String, value: Any) = {
    client.execute {
      search(index).matchQuery(field, value)
    }
  }




}
