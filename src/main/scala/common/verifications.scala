package common

import java.lang.Thread.sleep

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object verifications {


    def verify(test: Result, f:() => Future[List[check]]): Future[Result] ={

      if(test.timeWaitedMinutes < test.testConfig.timeAlottedMinutes )
        {
          val compare = f()
      compare flatMap { result =>
        test.addChecks(result)
        if(!allChecksPassed(result)) {

          sleep(test.testConfig.waitIntervalMinutes * 60000)
          test.timeWaitedMinutes += test.testConfig.waitIntervalMinutes
          test.adjustStatus()
          verify(test, f)
        }else
          {
            Future.successful{ test}
          }

      }
      }else
      {
        test.adjustStatus()
          Future{test}
      }
    }

  private def allChecksPassed(checks:List[check]): Boolean =
  {
    var allPassed = true
    for(check <- checks)
      {
        if(!check.result)
          {
            allPassed = false
          }
      }
    allPassed
  }






}
