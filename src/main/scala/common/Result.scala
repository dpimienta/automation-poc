package common

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class Result(var name:String) {
  var testStatus = "GREEN"
  var timeWaitedMinutes = 0
  var testConfig = new TestConfig()
  var checks:mutable.HashMap[String, ListBuffer[check]] = new mutable.HashMap[String, ListBuffer[check]]

  def this(name:String, test: TestConfig)
  {
    this(name)
    setTestConfig(test)
  }

  def addCheck(check:check): Unit =
  {
    if(!checks.contains(check.expected))
      {
        checks(check.expected) += check
      }else {
        checks(check.expected) = new ListBuffer[check]
         checks(check.expected) += check
    }
  }

  def addChecks(checksList:List[check]): Unit =
  {
    for(check<-checksList)
      {
        addCheck(check)
      }
  }

  def adjustStatus(): Unit =
  {
    if(timeWaitedMinutes <= testConfig.greenThresholdMinutes)
    {
      testStatus = "GREEN"
    }else if(timeWaitedMinutes < testConfig.redThresholdMinutes)
    {
      testStatus = "YELLOW"
    }else
    {
      testStatus = "RED"
    }
  }

  def setTestConfig(test: TestConfig)
  {
    this.testConfig = test
  }

}
