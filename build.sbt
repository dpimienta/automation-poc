name := "ScalaAuto"

version := "0.1"

scalaVersion := "2.12.3"

val elastic4sVersion = "5.6.0"
libraryDependencies ++= Seq(
  "com.sksamuel.elastic4s" %% "elastic4s-core" % elastic4sVersion,

  // for the http client
  "com.sksamuel.elastic4s" %% "elastic4s-http" % elastic4sVersion

)

libraryDependencies += "com.box" % "box-java-sdk" % "2.8.2"


libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.6"
libraryDependencies += "org.elasticsearch.client" % "elasticsearch-rest-client" % "5.6.2"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

excludeDependencies += "org.elasticsearch.client" % "transport" % "1.6.0"
excludeDependencies += "org.elasticsearch.client" % "transport" % "2.0.3"
excludeDependencies += "org.elasticsearch.client" % "transport" % "5.6.2"
excludeDependencies += "org.elasticsearch" %% "elasticsearch" % "5.6.2"
excludeDependencies += "org.apache.lucene" %% "lucene" % "6.6.1"


logBuffered in Test := true
javaOptions in Test += "-Dlogger.resource=logback-test.xml"
